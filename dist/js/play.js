var autoClick, autotest, config, dealer, leDeck, loadedGame, players;

autotest = 0;

loadedGame = new LaBataille();

leDeck = loadedGame.cards;

$('#state').html('sparti');

players = [
  new DeckPlayer({
    id: 0,
    name: "bob",
    type: "true-player"
  }), new DeckPlayer({
    id: 1,
    name: "abrasiveGuy",
    type: "true-player"
  })
];

config = {
  players: players,
  deck: leDeck,
  game: loadedGame
};

dealer = new DeckDealer(config);

dealer.shuffle();

dealer.distributeAll(players, 6);

dealer.jQActions(dealer);

dealer.play();

autoClick = function() {
  console.log("autoclick");
  return $('#input-choice button')[0].click();
};

if (autotest) {
  autoClick();
}
