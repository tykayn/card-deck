
/**
card player
@returns {undefined}
 */
var DeckPlayer;

DeckPlayer = (function() {
  DeckPlayer.prototype.type = "IA";

  DeckPlayer.prototype.cards = [];

  function DeckPlayer(config) {
    var attrname;
    this.id = 0;
    this.name = "";
    this.cards = [];
    this.cardsOrigin = [];
    this.stash = "";
    this.score = 0;
    this.victory = 0;
    this.turnId = "";
    this.type = "NPC";
    this.hasCards = function() {
      if (this.cards.length > 0) {
        return true;
      }
      return false;
    };
    this.dropHand = function() {
      var dropped;
      dropped = this.cards;
      this.cards = [];
      return dropped;
    };
    this.status = function() {
      var content, text;
      content = "player " + this.id + ") " + this.name + ". having <strong>" + this.cards.length + " </strong>cards <br/> <span class='score'>" + this.score + "</span> points";
      text = content;
      if (this.victory) {
        text = "<div class='alert-success alert'>" + content + "</div>";
      }
      return text;
    };
    for (attrname in config) {
      this[attrname] = config[attrname];
    }
    this;
  }

  return DeckPlayer;

})();
