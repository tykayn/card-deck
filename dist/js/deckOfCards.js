
/**
deck of cards
@returns {undefined}
 */
var Deck,
  __slice = [].slice;

Deck = (function() {
  function Deck() {}

  Deck.prototype.cards = [];

  Deck.prototype.hasDistributed = 0;

  Deck.prototype.graveyard = [];

  Deck.prototype.log = function() {
    var blah;
    blah = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    return console.log('===== ', blah);
  };

  return Deck;

})();
