var LaBataille,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

LaBataille = (function(_super) {
  __extends(LaBataille, _super);

  function LaBataille() {
    return LaBataille.__super__.constructor.apply(this, arguments);
  }

  LaBataille.prototype.name = "la bataille";

  LaBataille.prototype.type = "cartes 52, tour par tour";

  LaBataille.prototype.version = 0.2;

  LaBataille.prototype.shortDescription = "jeu de la bataille simple";

  LaBataille.prototype.author = "TyKayn";

  LaBataille.prototype.author_url = "http://github.com/tykayn";

  LaBataille.prototype.game_url = "http://github.com/tykayn/card-deck";

  LaBataille.prototype.players = {
    "default": 2,
    min: 2,
    max: 6
  };

  LaBataille.prototype.briefing = function() {
    var texte;
    texte = "";
    return texte;
  };

  LaBataille.prototype.buildCards = function() {
    var color, colors, colors_en, colors_icon, colors_icons, config, count, htmlIcon, i, j, names, points, values;
    values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, "V", "D", "R"];
    names = ["as", 2, 3, 4, 5, 6, 7, 8, 9, 10, "Valet", "Dame", "Roi"];
    colors = ["coeur", "trèfle", "carreau", "pique"];
    colors_en = ["heart", "club", "diamond", "spade"];
    colors_icons = ["♥", "♣", "♦", "♠"];
    htmlIcon = ["&hearts;", "&spades;", "&clubs;", "&diams;"];
    points = [14, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
    count = 0;
    i = 0;
    while (i < colors.length) {
      color = colors[i];
      colors_icon = colors_icons[i];
      j = 0;
      while (j < values.length) {
        config = {
          id: count,
          color: color,
          colors_icon: colors_icon,
          htmlIcon: htmlIcon[i],
          code: j + "-" + color.substring(0, 3),
          name: names[j] + " de " + color,
          points: points[j]
        };
        count++;
        this.addCard(new Card(config));
        j++;
      }
      i++;
    }
    return console.log("construction des " + count + " cartes de la bataille");
  };

  LaBataille.prototype.cards2html = function(cards) {
    var c, guyId, html, _i, _len;
    if (!cards) {
      return '';
    }
    html = '';
    for (_i = 0, _len = cards.length; _i < _len; _i++) {
      c = cards[_i];
      guyId = parseInt(this.playerActive);
      html += '<div class="' + this.oneCard.css + ' card card-' + c.color + ' score-';
      html += c.points + ' col-lg-1" data-id="' + c.id + '" data-playerid="';
      html += guyId + '">' + c.colors_icon + ' ' + c.name + '<span class="well points pull-left"> ' + c.points + '</span></div>';
    }
    return html;
  };

  return LaBataille;

})(DeckGame);
