var DeckGame;

DeckGame = (function() {
  DeckGame.prototype.name = 'My deck game';

  DeckGame.prototype.author = "nobody";

  DeckGame.prototype.author_url = "http://github.com/tykayn";

  DeckGame.prototype.game_url = "http://github.com/tykayn/card-deck";

  DeckGame.prototype.version = "0.1";

  DeckGame.prototype.shortDescription = 'my short game description';

  DeckGame.prototype.time = '5';

  DeckGame.prototype.oneCard = Card;

  DeckGame.prototype.rules = 'a deck game';

  DeckGame.prototype.type = 'type';

  DeckGame.prototype.nbCards = 52;

  DeckGame.prototype.nbTurns = 150;


  /*
    collections
   */

  DeckGame.prototype.cards = [];

  DeckGame.prototype.originalDeck = [];

  DeckGame.prototype.graveyard = [];

  DeckGame.prototype.other_objects = {
    dices: false,
    money: false,
    tokens: false
  };

  DeckGame.prototype.players = {
    "default": 2,
    min: 2,
    max: 4
  };

  DeckGame.prototype.computer = true;

  DeckGame.prototype.webSocket = true;


  /*
    cheminement du jeu
   */

  DeckGame.prototype.workflow = {
    shuffle: true,
    distributeAll: 6,
    start_player: 1,
    next_turn: [
      {
        put_card_on_table: 1,
        if_table_has: [2, 'fight_cards'],
        if_hand_equals: [0, 'win']
      }
    ]
  };

  DeckGame.prototype.dealerNewCompetences = [
    {
      name: 'mettre_une_baffe',
      action: function(joueur) {
        return console.log('le dealer met une baffe au joueur' + joueur.name);
      }
    }
  ];

  function DeckGame() {
    this.buildCards();
    this.autoLoad();
  }


  /*
    rendu html du workflow
   */

  DeckGame.prototype.workflowDisplay = function() {
    return this.workflow;
  };

  DeckGame.prototype.autoLoad = function() {
    var competence, functName, newFunc, _i, _len, _ref, _results;
    console.log("auto chargement du jeu", this.name);
    if (DeckDealer) {
      _ref = this.dealerNewCompetences;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        competence = _ref[_i];
        functName = competence.name;
        newFunc = competence.action;
        _results.push(DeckDealer[functName] = newFunc);
      }
      return _results;
    }
  };

  DeckGame.prototype.buildCards = function() {
    var color, colors, colors_en, colors_icon, colors_icons, config, count, htmlIcon, i, j, names, points, values, _results;
    values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, "V", "D", "R"];
    names = ["as", 2, 3, 4, 5, 6, 7, 8, 9, 10, "Valet", "Dame", "Roi"];
    colors = ["coeur", "trèfle", "carreau", "pique"];
    colors_en = ["heart", "club", "diamond", "spade"];
    colors_icons = ["♥", "♣", "♦", "♠"];
    htmlIcon = ["&hearts;", "&spades;", "&clubs;", "&diams;"];
    points = [14, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
    count = 0;
    i = 0;
    _results = [];
    while (i < colors.length) {
      color = colors[i];
      colors_icon = colors_icons[i];
      j = 0;
      while (j < values.length) {
        config = {
          id: count,
          color: color,
          colors_icon: colors_icon,
          htmlIcon: htmlIcon[i],
          code: j + "-" + color.substring(0, 3),
          name: names[j] + " de " + color,
          points: points[j]
        };
        count++;
        this.addCard(new Card(config));
        j++;
      }
      _results.push(i++);
    }
    return _results;
  };

  DeckGame.prototype.health = function() {
    var blah;
    blah = "i am a deck having " + this.cards.length + " cards.";
    return blah;
  };

  DeckGame.prototype.tellCards = function() {
    var i;
    i = 0;
    while (i < this.cards.length) {
      blah += "<br/>" + this.cards[i].name;
      i++;
    }
    return blah;
  };

  DeckGame.prototype.jQActions = function() {
    return console.log('XXXXXXXX pas d\'actions avec jquery');
  };

  DeckGame.prototype.removeCard = function(card) {
    var cardid, exitedCard;
    cardid = 12;
    exitedCard = this.cards.pop(cardid);
    this.graveyard.push(exitedCard);
  };

  DeckGame.prototype.cards2html = function(cards) {
    var c, guyId, html, _i, _len;
    if (!cards) {
      return '';
    }
    html = '';
    for (_i = 0, _len = cards.length; _i < _len; _i++) {
      c = cards[_i];
      guyId = parseInt(this.playerActive);
      html += '<button class="' + this.oneCard.css + ' card card-' + c.color + ' score-';
      html += c.points + ' col-lg-1" data-id="' + c.id + '" data-playerid="';
      html += guyId + '">' + c.name + '</button>';
    }
    return html;
  };

  DeckGame.prototype.addCard = function(card) {
    this.cards.push(card);
    this.originalDeck.push(card);
  };

  DeckGame.prototype.briefing = function() {
    return this.shortDescription;
  };

  return DeckGame;

})();
