class LaBataille extends DeckGame
  name: "la bataille"
  type: "cartes 52, tour par tour"
  version : 0.2
  shortDescription: "jeu de la bataille simple"
  author: "TyKayn"
  author_url: "http://github.com/tykayn"
  game_url: "http://github.com/tykayn/card-deck"
  players: {
    default: 2
    min: 2
    max: 6
  }
  briefing: ->
    texte = """

            """
#    console.log texte
    texte
  buildCards: ->
    values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, "V", "D", "R"]
    names = ["as", 2, 3, 4, 5, 6, 7, 8, 9, 10, "Valet", "Dame", "Roi"]
    colors = ["coeur", "trèfle", "carreau", "pique"]
    colors_en = ["heart", "club", "diamond", "spade"]
    colors_icons = ["♥","♣", "♦","♠"]
    htmlIcon = ["&hearts;", "&spades;", "&clubs;", "&diams;"]
    points = [14, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    count = 0
    i = 0
    while i < colors.length
      color = colors[i]
      colors_icon = colors_icons[i]
      j = 0
      while j < values.length
        config =
          id: count
          color: color
          colors_icon: colors_icon
          htmlIcon: htmlIcon[i]
          code: j + "-" + color.substring(0, 3)
          name: names[j] + " de " + color
          points: points[j]

        count++
        @addCard new Card(config)
        j++
      i++
    console.log("construction des " + count + " cartes de la bataille")
#render html view of a set of cards
  cards2html: (cards)->
#    console.log('cartes a rendre en html : ', cards)
    if not cards
      return ''
    html = ''
    for c in cards
      guyId = parseInt(@playerActive)
      html += '<div class="'+@oneCard.css+' card card-' + c.color + ' score-'
      html += c.points + ' col-lg-1" data-id="' + c.id + '" data-playerid="'
      html += guyId + '">'+c.colors_icon+' ' + c.name + '<span class="well points pull-left"> ' + c.points + '</span></div>'
    html