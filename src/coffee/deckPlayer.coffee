
###*
card player
@returns {undefined}
###
class DeckPlayer
  type : "IA" # intelligence artificielle / true-player
  cards : []
  constructor: (config) ->
    @id = 0
    @name = ""
    @cards = []
    @cardsOrigin = []
    @stash = "" # a place to store cards that wont be used
    @score = 0
    @victory = 0
    @turnId = ""
    @type = "NPC" # Non playing character or true player
    @hasCards = ->
      return true  if @cards.length > 0
      false
      # vider la main
    @dropHand = ->
#      dropped = Object.clone(@cards);
      dropped =  @cards;
      @cards = [];
      dropped
    @status = ->
      content = "player " + @id + ") " + @name + ". having <strong>" + @cards.length + " </strong>cards <br/> <span class='score'>" + @score + "</span> points"
      text = content
      if @victory
        text = "<div class='alert-success alert'>"+content+"</div>"
      text

    for attrname of config
      this[attrname] = config[attrname]
    this