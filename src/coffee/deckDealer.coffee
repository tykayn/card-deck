###
core of the action in a game,
the dealer runs the turns
@returns {undefined}
###
class DeckDealer
  jeux: []
  players: []
  deck: {}
  playerToStart: 0
  playerActive: {}
  jQActions : (TheDealer = {})->
    TheDealer = @
    if TheDealer.deck.length
      console.log('------- jqactions avec TheDealer')
    if not $
      console.error('ce jeu nécessite jquery pour fonctionner, vérifiez qu\'il est chargé')
#    console.log('------- TheDealer activeGuy' , TheDealer.activeGuy)
    $("body").on("click", "#input-choice .card", (e)->
  # "this" becomes the clicked button
      cardClicked = $(@)
      #      name = cardClicked.attr("data-playerid")
      cardId = cardClicked.attr("data-id")
      hand = TheDealer.activeGuy.cards
      card = TheDealer.idToCard(cardId, hand)
#      console.log "carte a poser: ", card.name
      TheDealer.putCardToTable(card)
      cardClicked.fadeOut()
    )
    # lister les jeux
    listeJeux = '<ul>'
    for jeu in dealer.jeux
      listeJeux += '<li>'+jeu.name+'</li>'
    listeJeux += '</ul>'
    $('#listeJeux').html(listeJeux)
#    console.log('------- jQActions ajoutées')

  constructor: (config = {}) ->
    console.log('new game dealer')
    @setPlayers(config.players) if config.players
#    @deck = config.deck if config.deck
    if config.game
      # hériter des actions jQuery afin d'avoir accès aux joueurs
      dealer = @
#      @jQActions = config.game.jQActions(dealer)
      @setGame(config.game)
#      @jQActions(@)
    console.log('card dealer built with '+@deck.length+' cards')

# config of game environment
  config:
    autoplay: 1
    activeGuy: {}
    graveyard: []
    table: []
    otherPlayer: {} # player to compare scores with
  table: [] # place where cards are shown to everyone
  graveyard: [] # place where cards go when out of the game. RIP.
  maxTableTurns: 20
  maxTurns: 200
  turn: 0
  game: {}

  setPlayers : (players)->
    @players = players
    @log('les '+players.length+' joueurs prennent place')
  # load a game and its cards
  setGame: (newGame)->
    console.log('set game ')
    if !newGame
      console.error('nouveau jeu invalide: ')
      console.error(newGame)
      return false
#    @addGame(newGame)
    @game = newGame
    @deck = newGame.cards
    console.log('newGame.cards ' , newGame.cards.length)
    @activeGuy = @players[@playerToStart];
    $('#rulesInfo').html(@game.briefing())
    $('.GameName').html(@game.name)
#    console.log('@activeGuy ' , @activeGuy.name)
#    console.log('nouveau jeu défini: ' + newGame.name)
# ask active player to do something
  deck2html :(cards) ->
    @game.cards2html(cards)
# ask active player to do something
  askInput: (message = '<br/>à vous de jouer '+@activeGuy.name)->
    @activeGuy = @players[@playerActive];
    activeName = @activeGuy.name

    #      $('#input-instructions').html( activeName+' play a card with a high value')
    #      list the cards of the player.
    cards = []
    if @activeGuy
      cards = @activeGuy.cards
      choice = ''
      choice = @deck2html(cards)
      $('#input-choice').html(choice)
    # console.log('en attente du joueur: '+activeName)
    @setState('<h2>' + @turn + '</h2> statoi de jouer, ' + activeName + message)

  #set a text in the state of the game
  setState: (text)->
    $('#state').html(text)

  # ajouter à la liste des jeux connus
  addGame: (game)->
    # vérifier que le jeu n'est pas déja là
    for g in @jeux
      if g.name is game.name
        return false
    # autrement, l'ajouter
    @jeux.push(game)
  # shuffle the deck
  shuffle: ->
#    console.log('shuffle', @deck.length)
    o = @deck
    if not o
      return o
    j = undefined
    x = undefined
    i = o.length
    while i
      j = Math.floor(Math.random() * i)
      x = o[--i]
      o[i] = o[j]
      o[j] = x
    @deck = o
#    console.log('shuffle', @deck.length)

    @deck

  # distribute a certain number of cards to one player
  distribute: (player, int) ->

    #      # console.log 'le deck avait '+@deck.length+' cartes'
    #      # console.log 'distribution de carte à '+player.name+' (qui a '+player.cards.length+' cartes)'
    i = 0
    if int is 0
      return
    if @deck.length is 0
      return 0
    while i < int
      oneCard = @deck.pop()
      oneCard.ownerId = i
      player.cards.push oneCard
      i++
    @hasDistributed = 1
    i # return the number of cards distributed
  ###
    distribute an equal number of cards to all the players
  ###
  distributeAll: (players, int) ->
    console.log('le deck avait '+@deck.length+' cartes')
    if not @deck
      console.error 'pas de deck chez le dealer'
      return false
    i =0
    for p in @players

      if typeof (p) isnt `undefined`
        @log 'distribution de ' + int + ' cartes à ' + p.name
        j = 0
        while j < int
          oneCard = @deck.pop()
          oneCard.ownerId = i
          p.cards.push oneCard
          j++
        console.log '    il a maintenant '+p.cards.length+' cartes'
      i++
    @hasDistributed = 1
    @upPlayers()
    true
  #show players in the view
  upPlayers : ()->
    for p in @players
      $('#player-'+p.id).html('<strong>'+p.name+'</strong> '+p.cards.length+' cartes')
  # console.log('vue mise a jour')
  # put all cards of the table in the graveyard
  emptyTable: ->
    for card in @table
      @discard(card)
    @table = []
    # console.log('table is now empty')
  # mettre une carte au cimetière
  discard:(card)->
    @game.graveyard.push(card)
  nextTurn: ->
    @turn++
    # take a fight of the cards only when there are 2 cards on the table
    if @table.length == 2
      fightResult = @tableFight()
      if fightResult == "equal"
        @log('égalité!')
      else
        @players[fightResult].score++;
        @log(@players[fightResult].name + ' a gagné le match!')
      @emptyTable()

    @isItFinished()


# check if the game is over
# has current player won and has no cards left in his hands ?
  isItFinished: ->
    # # console.log "checking if the game is over, turn: "+@turn+" / "+@maxTurns
    if @maxTurns < @turn
      return @gameOver()

    if @activeGuy && @activeGuy.cards.length is 0
      return @winning()
    # continue game
    else
      @setActivePlayer()
      @refreshView()
      if @config.autoplay
        if @activeGuy.type == "NPC"
        ## console.log " NPC spotted "+@activeGuy.name
          return setTimeout(@autoplay(), 500)
        else
      ## console.log " TRUE PLAYER spotted "+@activeGuy.name
      else
        return @askInput()

  #end of the game
  gameOver: ->
    @log(' maximum turns reached, game over')
    $("body").off("click", "#input-choice .card")
    $("#input-choice button").disable()
  # makes a non playing character play automatically
  autoplay: ->
  # console.log(' AUTOPLAY '+@activeGuy.name+':')
  # a way to choose a card in the hand
    card = @activeGuy.cards.pop(0);
#    card = @activeGuy.cards[0];
    @refreshView()
    @putCardToTable(card)


  winning: ->
    $("#input-choice, #table").fadeOut()
    txt = 'le joueur ' + @activeGuy.name + ' est vainqueur!'
    @activeGuy.won = 1
    # console.log(txt)
    @log(txt)
    $("#state").html txt

  oneTurn: ->
    @refreshView()
    @askInput()


# check for cards of a player,
# remove the one we are looking for and return it
# needle is the id of the card
# haystack is the card array
  idToCard: (needle, haystack)->
    needle = parseInt(needle)
    # console.log('we are looking for an id of', needle)
    i = 0
    for c in haystack
      if ( parseInt(c.id) == needle)
#        console.log('card found')
        return c
    console.log('card '+needle+' NOT found')
    i++

  idToHandId: (needle, haystack)->
    needle = parseInt(needle)
    #      # console.log('we are looking for an id of', needle, haystack)
    i = 0
    for c in haystack
      if ( parseInt(c.id) == needle)
        return i
      i++
  getGame: ->
    @game
  # run all the turns
  play: ->

    @maxTurns = (@maxTableTurns * players.length)
    @activeGuy = @players[@playerToStart]
    @playerActive = @playerToStart
#    console.log('******play sparti', @players,@playerToStart,@activeGuy)
    log = ""
    i = 1
    @oneTurn()
  # put a card in the table array
  # and return
  table : []
  killAllHands : ->
    for p in @players
      @killPlayerHand(p)
  killPlayerHand: (player)->
    droppedCards = player.cards
    player.cards = []
    @graveyard.push droppedCards

  putCardToTable: (card)->
    card.ownerId = @activeGuy.id if @activeGuy
    # remove the card from active player's hand
    res = @idToHandId(card.id, @activeGuy.cards)
    @activeGuy.cards.splice(res, 1)
    @table.push card
    # only if it is a true player
    if @activeGuy.type == "true-player"
      console.log('   le joueur a maintenant ' + @activeGuy.cards.length + ' cartes')
    @log('le joueur ' + @activeGuy.name + ' pose la carte ' + card.name)
    @nextTurn()
  # fight between two players
  # returns the winner id or the string "equal"
  tableFightGo: ()->
    firstPoints = @table[0].points
    firstId = @table[0].ownerId
    SecondPoints = @table[1].points
    SecondId = @table[0].ownerId
#    console.log('tableFight finished')
    if( firstPoints is SecondPoints )
      return "equal"
    else
      if( firstPoints > SecondPoints )
        @loserAction(SecondId)
        return firstId
      else
        @loserAction(firstId)
        return SecondId
# comparaison
# case of equal strength
  tableFight: ()->
#    console.log('tableFight...')
    return @tableFightGo()


# triggered after a table fight
  loserAction: (idLoser)->
# make the loser player pick a card from the deck
    theGuy = @players[idLoser]
    result = @distribute(theGuy, 1)
    if result == 1
# console.log("le joueur "+@players[idLoser].name+" pioche une carte")
    else
      if result == 0
# console.log("le joueur "+@players[idLoser].name+" n'a PAS pu pioche une carte")
        if @players[idLoser].cards.length == 0
          @winning()

# set who's turn it is to play
  setActivePlayer: ->
    @playerActive++
    @playerActive = 0  if @playerActive >= players.length
    @activeGuy = @players[@playerActive]
#    console.log "le joueur actif est maintenant "+@activeGuy.name

  log: (text) ->
    if @turn != @lastTurn
      text = " <h2>tour " + @turn + "</h2> " + text
    text = "<div class=\"bs-callin bs-callin-info\"><p>" + text + "</p></div>"
    $("#log").append text
    @lastTurn = @turn

  refreshView: () ->
    players = @players
    $("#table").html @deck2html(@table)
    $("#graveyardLength").html @game.graveyard.length
    if @game
      status = @game.health()
      $("#state").html status
    $("#graveyard").html @deck2html(@game.graveyard)
    if @activeGuy
      $('#input-instructions').html(@activeGuy.name + ', please play a card with a high value.')
      #      list the cards of the player.
      cards = []
      cards = @activeGuy.cards
      choice = ''
      choice = @deck2html(cards)
      $('#activeGuyName').html('<h2>Main de ' + @activeGuy.name + '</h2>' )
      $('#input-choice').html( choice)
    tempCount = 0
    while tempCount < @players.length
      $("#player-" + tempCount).html players[tempCount].status()
      tempCount++
