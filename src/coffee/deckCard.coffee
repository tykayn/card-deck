###*
any card
@returns {undefined}
###
class Card
  id : null
  ownerId : undefined
  code : ""
  name : ""
  color : ""
  points : ""
  css : "card" # classe affichée
  constructor : (config)->
    for attrname of config
      this[attrname] = config[attrname]
    this

