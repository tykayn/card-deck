var gulp = require("gulp");
var gutil = require("gulp-util");
var plumber = require("gulp-plumber");
var myth = require("gulp-myth");
var csso = require("gulp-csso");
var coffee = require("gulp-coffee");
var options = require("minimist")(process.argv.slice(2));
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var karma = require('karma').server;
var jshint = require('gulp-jshint');
var istanbul = require('gulp-istanbul');
// We'll use mocha here, but any test framework will work
var mocha = require('gulp-mocha');
var testSuite = ['src/tests/*.js'];
var filesToTest = [
  'dist/js/deckPlayer.js',
  'dist/js/deckDealer.js',
  'dist/js/deckCard.js',
  'dist/js/deckGame.js',
  'dist/js/deckOfCards.js',
  'dist/js/jeux/bataille.js',
  'dist/js/play.js',
  'dist/js/jeux/deckApp.js'
];

/**
 * rien
 */
gulp.task('rien', function (cb) {

  console.log('riennnnnnnn');
});
/**
 * Run test once and exit
 */
gulp.task('test', function (cb) {
  console.log('----- testing');
  karma.start({
    configFile: __dirname + '/karma.conf.js',
    action    : 'run'
  });
  console.log('----- istanbul done');
});

/**
 * Watch for file changes and re-run tests on each change
 */
gulp.task('tdd', function (done) {
  karma.start({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, done);
});

var sources = {
  tests  : "src/tests/*.js",
  sass   : "src/sass/*.scss",
  html   : "src/html/*.html",
  coffee : "src/coffee/*.coffee",
  coffees: "src/coffee/**/*.coffee"
};
var destinations = {
  sass  : "dist/sass/",
  html  : "dist/html/",
  coffee: "dist/coffee/"
};

gulp.task("styles", function () {
  gulp.src("./src/css/*.css")
    .pipe(options.production ? plumber() : gutil.noop())
    .pipe(myth({sourcemap: !options.production}))
    .pipe(options.production ? csso() : gutil.noop())
    .pipe(gulp.dest("./dist/css/"));
});
gulp.task("hello", function () {
  console.log("hello le monde!");
});
gulp.task('browser-sync', function () {
  return browserSync.init(null, {
    open  : false,
    server: {
      baseDir: "./dist"
    }
  });
});
gulp.task("html", function () {
  console.log("html was changed");
  gulp.src("./src/html/*.html")
    .pipe(gulp.dest("./dist/"))
    .pipe(reload({stream: true}));
});
gulp.task("sass2css", function () {
  gulp.src("./src/sass/*.scss")
    .pipe(sass({outputStyle: 'compressed', errLogToConsole: true}))
    .pipe(gulp.dest("./dist/css/"))
    .pipe(reload({stream: true}))
    .on('error', gutil.log);
});
gulp.task("coffee2js", function () {
  gulp.src(["./src/coffee/*.coffee"])
    .pipe(plumber())
    .pipe(coffee({bare: true}))
    .pipe(gulp.dest("./dist/js/"));
  gulp.src(["./src/coffee/jeux/*.coffee"])
    .pipe(plumber())
    .pipe(coffee({bare: true}))
    .pipe(gulp.dest("./dist/js/jeux/"))
    .pipe(reload({stream: true}))
});
gulp.task('lint', function () {
  return gulp.src('./lib/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});
gulp.task('watch', function () {
  gulp.watch(sources.tests, ['test']);
  gulp.watch(sources.sass, ['sass2css']);
  gulp.watch(sources.html, ['html']);
  gulp.watch(sources.coffee, ['coffee2js']);
  gulp.watch(sources.coffees, ['coffee2js']);

});
gulp.task("default", ["coffee2js", "lint", "sass2css", "html", "browser-sync", "watch", "tdd"], function () {
  console.log("spartiiiii");
});
